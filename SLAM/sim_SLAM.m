%% 1. Initialise Workspace and Run Tests
clear;clc;close all
mex Mainmex.c raycast.c
% Run unit tests for function
testResults = runtests('Neato_func_test.m');
assert(all([testResults(:).Passed]))

%% 2. Initialise all parameters
mapSize = 100;                  %in grid squares
mapSizeM = mapSize*25.4/1000;   %in metres
param.max_range = sqrt(2)*1000*mapSize; %This is to ensure max reading is never larger than the diag. of the map
param.cell_size = 25.4;         %mm = 1 inch
param.old_map = keyed_square_map_of_probabilities(mapSize); %Retrieves manually built probability map to be able to read
param.new_map = ones(mapSize)*log10(0.4); %Initialise the new map with even probability log odds (Blank Map)

% Model parameters
param.Wb = 0.015;           %Wheel Base
param.Wr = 0.005;           %Wheel Radius
param.L = 0.03;             %Length to sensor

% Simulation parameters
T = 0.1;
t = 0:T:20;
param.dt = T; %time step

% Set initial condition
northI  = 2*mapSizeM/3; ...          % Initial North
eastI   = 2*mapSizeM/3; ...          % Initial East
psiI    = 0;

pose0 = [ ...
        northI; ...         % Initial north
        eastI; ...          % Initial east
        psiI;  ...          % Initial psi
     ];

[param.mapC, param.size_x, param.size_y] = map_for_c(param.old_map); %Transfers the map matrix to an array with x and y dimensions

nt  = length(t);        %Total simulation length
u   = [0;0];            %Initial inputs


theta=1:360;            %Initialse angle measurements

%% 3. Take initial readings from map through the raycasting function

[y,~,~]=measurementModelNeato(pose0,u,param);   % Retrieve the raycasts from the initial position
y=y*1000/param.cell_size;                       % Convert to inches


%% 4. Building the new map

for j=1:2
    for i=1:length(theta)                       % Iterates through all readings
        theta_2 = theta(i)+rad2deg(psiI);       % Accounts for psi offset
        r=round(y(i));                          % Sets range to be in inches
        
        % Calling function to compute free and occupied grid spaces from the measurement model
        [Myfree,Mxfree,Myocc,Mxocc] = free_occ_coord(pose0(1)*1000/param.cell_size,pose0(2)*1000/param.cell_size,r,theta_2);

        % Calling function to update grid occupancies
        param.new_map = Map_update(round(Myfree),round(Mxfree),round(Myocc),round(Mxocc),param.new_map,mapSize); 
    end
end

figure(1)
colormap(flipud(bone));
h = pcolor(param.new_map);                  % Plots the new occupancy map
set(h, 'EdgeColor', 'none');                % Removes the grid lines
mapshow(pose0(2)*1000/param.cell_size,pose0(1)*1000/param.cell_size,'Marker','x');    % Shows the actual position of the robot inside of map

%% 5. Initial Localistion using UKF

% Initial Guess State for the UKF
mu0 = [ ...
    2*mapSizeM/3 - 500e-3; ...          % Initial North
    2*mapSizeM/3 - 400e-3; ...          % Initial East
    deg2rad(1) ...                      % Initial psi
    ];

muUKF = mu0;            %Initial guess state put in the estimate variable

% Initial estimation error covariance
S = diag([0.1,0.1,deg2rad(3)]); % Upper Cholesky factor
assert(isequal(S,triu(S)));
SUKF = S; % Upper Cholesky decomposition

[param.mapC, param.size_x, param.size_y] = map_for_c(param.new_map);   %Transfer new map matrix to an array to be passed to C

for i = 1:100
    %UKF CALCULATIONS
    [muUKF,SUKF] = UKF(muUKF,SUKF,u,y*param.cell_size/1000,@processModelNeato,@measurementModelNeato,param);
    muHistUKF(:,i) = muUKF;
    SdiagHistUKF(:,i) = realsqrt(diag(SUKF.'*SUKF)); % Square root of diagonal of P
end

figure(2)
colormap(flipud(bone));
h = pcolor(param.new_map);                              % Plots the occupancy map
set(h, 'EdgeColor', 'none');                % Removes the grid lines
mapshow(muUKF(2)*1000/param.cell_size,muUKF(1)*1000/param.cell_size,'Marker','x');    % Shows position of the robot

%% 6. SLAM iteration 

u = [1;1];          %Wheel inputs for SLAM

for i=1:25
    %Movement Step
    pose=processModelNeato(muUKF,u,param);      %Move forward a timestep
    
    %Raycast Step
    [param.mapC, param.size_x, param.size_y] = map_for_c(param.old_map);    %Transfer real map matrix to an array to be passed to C
    [y,~,~]=measurementModelNeato(pose,u,param);        % Get new range readings
    y=y*1000/param.cell_size;           %Trasfer to inches (cell sizes)
    
    %Map update step
    for k=1:length(theta)                                     %Iterates through all readings
        theta_2 = theta(k);
        r=round(y(k));                %Sets range to be in inches
        % Calling function to compute free and occupied grid spaces from the measurement model
        [Myfree,Mxfree,Myocc,Mxocc] = free_occ_coord(pose(1)*1000/param.cell_size,pose(2)*1000/param.cell_size,r,theta_2);
        % Calling function to update grid occupancies
        param.new_map = Map_update(round(Myfree),round(Mxfree),round(Myocc),round(Mxocc),param.new_map,mapSize); 
    end
    
    %Localisation Step
    [param.mapC, param.size_x, param.size_y] = map_for_c(param.new_map); %Transfer new map matrix to an array to be passed to C
    for k = 1:20
        [muUKF,SUKF] = UKF(muUKF,SUKF,u,y*param.cell_size/1000,@processModelNeato,@measurementModelNeato,param);
        muHistUKF(:,k) = muUKF;
        SdiagHistUKF(:,k) = realsqrt(diag(SUKF.'*SUKF)); % Square root of diagonal of P
    end
    
    %Plotting Step
    figure(3);
    colormap(flipud(bone));
    h = pcolor(param.new_map);                  % Plots the occupancy map
    set(h, 'EdgeColor', 'none');                % Removes the grid lines
    mapshow(muUKF(2)*1000/param.cell_size,muUKF(1)*1000/param.cell_size,'Marker','x');    % Shows position of the robot
end             