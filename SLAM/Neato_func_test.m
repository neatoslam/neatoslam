%% Main function to generate tests
function tests = Neato_func_test
tests = functiontests(localfunctions);
end

%% Test Functions
function testNoInput(testCase)
% Parameters
param.Wb = 1;           %Wheel Base
param.Wr = 1;           %Wheel Radius
param.L = 11;             %Length to sensor
% Inputs
wheelL = 0;
wheelR = 0;
u = [wheelL; wheelR];
% States
north   = 0;    % [m]
east    = 0;    % [m]
psi     = 0;    % [rad]
x = [east; north; psi];

actual = Neato_func(u,x,param);
expected = [0; 0; 0];
verifyEqual(testCase, actual, expected, 'AbsTol', 10*eps, ...
    'Expected Zero Velocity');
end

function testStraightForwardNorth(testCase)
% Parameters
param.Wb = 10;           %Wheel Base
param.Wr = 1;           %Wheel Radius
param.L = 1;             %Length to sensor
% Inputs
wheelL = 1/param.Wr;
wheelR = 1/param.Wr;
u = [wheelL; wheelR];
% States
north   = 0;    % [m]
east    = 0;    % [m]
psi     = 0;    % [rad]
x = [east; north; psi];

actual = Neato_func(u,x,param);
expected = [1; 0; 0];
verifyEqual(testCase, actual, expected, 'AbsTol', 10*eps, ...
    'Expected Velocity Only in North');
end

function testStraightForwardEast(testCase)
% Parameters
param.Wb = 1;           %Wheel Base
param.Wr = 9;           %Wheel Radius
param.L = 1;             %Length to sensor
% Inputs
wheelL = 1/param.Wr;
wheelR = 1/param.Wr;
u = [wheelL; wheelR];
% States
north   = 0;    % [m]
east    = 0;    % [m]
psi     = pi/2;    % [rad]
x = [east; north; psi];

actual = Neato_func(u,x,param);
expected = [0; 1; 0];
verifyEqual(testCase, actual, expected, 'AbsTol', 10*eps, ...
    'Expected Velocity Only in East');
end

function testStraightForwardSouth(testCase)
% Parameters
param.Wb = 1;           %Wheel Base
param.Wr = 1;           %Wheel Radius
param.L = 6;             %Length to sensor
% Inputs
wheelL = 1/param.Wr;
wheelR = 1/param.Wr;
u = [wheelL; wheelR];
% States
north   = 0;    % [m]
east    = 0;    % [m]
psi     = -pi;    % [rad]
x = [east; north; psi];

actual = Neato_func(u,x,param);
expected = [-1; 0; 0];
verifyEqual(testCase, actual, expected, 'AbsTol', 10*eps, ...
    'Expected Velocity Only in Negative North');
end

function testStraightForwardWest(testCase)
% Parameters
param.Wb = 8;           %Wheel Base
param.Wr = 1;           %Wheel Radius
param.L = 1;             %Length to sensor
% Inputs
wheelL = 1/param.Wr;
wheelR = 1/param.Wr;
u = [wheelL; wheelR];
% States
north   = 0;    % [m]
east    = 0;    % [m]
psi     = -pi/2;    % [rad]
x = [east; north; psi];

actual = Neato_func(u,x,param);
expected = [0; -1; 0];
verifyEqual(testCase, actual, expected, 'AbsTol', 10*eps, ...
    'Expected Velocity Only in Negative East');
end

function testStraightForwardNorthEast(testCase)
% Parameters
param.Wb = 1;           %Wheel Base
param.Wr = 7;           %Wheel Radius
param.L = 1;             %Length to sensor
% Inputs
wheelL = 1/param.Wr;
wheelR = 1/param.Wr;
u = [wheelL; wheelR];
% States
north   = 0;    % [m]
east    = 0;    % [m]
psi     = pi/4;    % [rad]
x = [east; north; psi];

actual = Neato_func(u,x,param);
expected = [1/sqrt(2); 1/sqrt(2); 0];
verifyEqual(testCase, actual, expected, 'AbsTol', 10*eps, ...
    'Expected Velocity Equal in North and East');
end

function testSpinFacingNorth(testCase)
% Parameters
param.Wb = 3;           %Wheel Base
param.Wr = 6;           %Wheel Radius
param.L = 1;             %Length to sensor
% Inputs
wheelL = 1/param.Wr;
wheelR = -1/param.Wr;
u = [wheelL; wheelR];
% States
north   = 0;    % [m]
east    = 0;    % [m]
psi     = 0;    % [rad]
x = [east; north; psi];

actual = Neato_func(u,x,param);
expected = [0; -param.L/param.Wb; 1/(param.Wb)];
verifyEqual(testCase, actual, expected, 'AbsTol', 10*eps, ...
    'Expected Velocity only in Psi?');
end

function testSpinFacingEast(testCase)
% Parameters
param.Wb = 1;           %Wheel Base
param.Wr = 4;           %Wheel Radius
param.L = 5;             %Length to sensor
% Inputs
wheelL = 1/param.Wr;
wheelR = -1/param.Wr;
u = [wheelL; wheelR];
% States
north   = 0;    % [m]
east    = 0;    % [m]
psi     = pi/2;    % [rad]
x = [east; north; psi];

actual = Neato_func(u,x,param);
expected = [param.L/param.Wb; 0; 1/(param.Wb)];
verifyEqual(testCase, actual, expected, 'AbsTol', 10*eps, ...
    'Expected Velocity only in Psi?');
end

function testNegativeSpinFacingNorth(testCase)
% Parameters
param.Wb = 2;           %Wheel Base
param.Wr = 1;           %Wheel Radius
param.L = 30;             %Length to sensor
% Inputs
wheelL = -1/param.Wr;
wheelR = 1/param.Wr;
u = [wheelL; wheelR];
% States
north   = 0;    % [m]
east    = 0;    % [m]
psi     = 0;    % [rad]
x = [east; north; psi];

actual = Neato_func(u,x,param);
expected = [0; param.L/param.Wb; -1/(param.Wb)];
verifyEqual(testCase, actual, expected, 'AbsTol', 10*eps, ...
    'Expected Velocity only in -Psi?');
end

%% Optional file fixtures  
function setupOnce(testCase)  % do not change function name
end

function teardownOnce(testCase)  % do not change function name
end

%% Optional fresh fixtures  
function setup(testCase)  % do not change function name
end

function teardown(testCase)  % do not change function name
end