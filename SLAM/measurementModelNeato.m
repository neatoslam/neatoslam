 function [y,C,SR] = measurementModelNeato(pose,u,param)
%#codegen

%parameters
dt = param.dt; %time step
Wb = param.Wb; %wheel base
Wr = param.Wr; %wheel radius
L  = param.L;  %length to sensor

mapC    = param.mapC;   %map in array form for C mexing
size_x  = param.size_x; %Columns of mapC
size_y  = param.size_y; %Rows of mapC

%states THESE WERE CHANGED
north   = pose(2,:);
east    = pose(1,:);
psi     = pose(3,:);

%inputs
wheelL = u(1);
wheelR = u(2);

raygap = 1; 

% measurements

%C 2(MEX) Version
theta = deg2rad(raygap:raygap:360);
ny = length(theta);
np = length(north);
y = Mainmex(size_x, size_y, param.max_range, param.cell_size, mapC', theta, north, east, psi);
y = y';
y=reshape(y,[ny np]);

% % MATLAB Version
% y = param.max_range*ones(360/raygap, 1);
% 
% theta = deg2rad(raygap:raygap:360)';
%
% for i = 1:360/raygap
%     
%     y(i) = Ray_cast(pose, theta(i), param);
% 
% end
 

% This is the linearisation about x (for EKF)
C = [];

% Square root of measurement noise covariance
SR = 25.4e-3*eye(ny);

