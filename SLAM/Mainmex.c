//----------------------------------------------------------------------------------//
//---------MAIN MEX FUNCTION TO BRING IN VARIABLES AND CALL OTHER FUNCTIONS---------//
//----------------------------------------------------------------------------------//
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include "mex.h"
#include "map.h"

//---The gateway routine---//
void mexFunction(int nlhs,mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double *map, max_range, cell_size, *theta, *north, *east, *psi, *z;             /* input scalar */
    int size_x, size_y;
    size_t ncols;                                           /* size of array */
 

    /* check for proper number of arguments */
    if(nrhs!=9) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nrhs","5 inputs required.");
    }
    if(nlhs!=1) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nlhs","One output required.");
    }
    /* make sure the first input argument is scalar */
    if( !mxIsDouble(prhs[0]) || 
         mxIsComplex(prhs[0]) ||
         mxGetNumberOfElements(prhs[0])!=1 ){
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar","First argument must be a scalar.");
    }
    
    /* make sure the second input argument is scalar */
    if( !mxIsDouble(prhs[1]) || 
         mxIsComplex(prhs[1]) ||
         mxGetNumberOfElements(prhs[1])!=1 ){
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar","Second argument must be a scalar.");
    }
    
    /* make sure the third input argument is scalar */
    if( !mxIsDouble(prhs[2]) || 
         mxIsComplex(prhs[2]) ||
         mxGetNumberOfElements(prhs[2])!=1 ){
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar","Third argument must be a scalar.");
    }

    /* make sure the fourth input argument is scalar */
    if( !mxIsDouble(prhs[3]) || 
         mxIsComplex(prhs[3]) ||
         mxGetNumberOfElements(prhs[3])!=1 ){
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar","fourth argument must be a scalar.");
    }
    
    
    /* make sure the 5th input argument is a matrix */
    if( !mxIsDouble(prhs[4]) || 
         mxIsComplex(prhs[4])) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notDouble","First input matrix must be type double.");
    }
    
    /* make sure the 6th input argument is a matrix */
    if( !mxIsDouble(prhs[5]) || 
         mxIsComplex(prhs[5])) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notDouble","Second Input matrix must be type double.");
    }
    
    /* make sure the 7th input argument is a matrix */
    if( !mxIsDouble(prhs[6]) || 
         mxIsComplex(prhs[6])) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notDouble","Third matrix must be type double.");
    }
    
    /* make sure the 8th input argument is a matrix */
    if( !mxIsDouble(prhs[7]) || 
         mxIsComplex(prhs[7])) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notDouble","Fourth matrix must be type double.");
    }
    
    /* make sure the 9th input argument is a matrix */
    if( !mxIsDouble(prhs[8]) || 
         mxIsComplex(prhs[8])) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notDouble","Fifth matrix must be type double.");
    }
    
    /* check that number of rows in the 5th input argument is 1 */
    if(mxGetM(prhs[4])!=1) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","1st matrix must be a row vector.");
    }

    /* check that number of rows in the 6th input argument is 1 */
    if(mxGetM(prhs[5])!=1) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","2nd matrix must be a row vector.");
    }
    
    /* check that number of rows in the 7th input argument is 1 */
    if(mxGetM(prhs[6])!=1) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","3rd matrix must be a row vector.");
    }
    
    /* check that number of rows in the 8th input argument is 1 */
    if(mxGetM(prhs[7])!=1) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","4th matrix must be a row vector.");
    }
    
    /* check that number of rows in the 9th input argument is 1 */
    if(mxGetM(prhs[8])!=1) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","5th matrix must be a row vector.");
    }
    
    
    /* get the value of the 4 scalar inputs */
    size_x = mxGetScalar(prhs[0]);           /* x dimension of the map */
    size_y = mxGetScalar(prhs[1]);           /* y dimension of the map */
    max_range = mxGetScalar(prhs[2]);   /* max_range measurement of an error */
    cell_size = mxGetScalar(prhs[3]);   /* cell size of the grid map */
    
    /* ARRAY INPUTS ARE PROCESSED BELOW */
    /* create a pointer to the real data in the input matrix, being used here for a scalar  */
    #if MX_HAS_INTERLEAVED_COMPLEX
    map = mxGetDoubles(prhs[4]);
    #else
    map = mxGetPr(prhs[4]);
    #endif
    
    #if MX_HAS_INTERLEAVED_COMPLEX
    theta = mxGetDoubles(prhs[5]);
    #else
    theta = mxGetPr(prhs[5]);
    #endif
    
    #if MX_HAS_INTERLEAVED_COMPLEX
    north = mxGetDoubles(prhs[6]);
    #else
    north = mxGetPr(prhs[6]);
    #endif
    
    #if MX_HAS_INTERLEAVED_COMPLEX
    east = mxGetDoubles(prhs[7]);
    #else
    east = mxGetPr(prhs[7]);
    #endif
    
    #if MX_HAS_INTERLEAVED_COMPLEX
    psi = mxGetDoubles(prhs[8]);
    #else
    psi = mxGetPr(prhs[8]);
    #endif
    
    /* Number of columns for output */
    ncols = mxGetN(prhs[5])*mxGetN(prhs[6]);
    plhs[0] = mxCreateDoubleMatrix(1,(mwSize)ncols,mxREAL);
    
    /* get a pointer to the real data in the output matrix, used as an output scalar in this case */
    #if MX_HAS_INTERLEAVED_COMPLEX
    z = mxGetDoubles(plhs[0]);
    #else
    z = mxGetPr(plhs[0]);
    #endif

    /* call the computational routine */
       //

    int Particles = mxGetN(prhs[6]);//sizeof(north);
      
    int scanSize = mxGetN(prhs[5]);//sizeof(theta);
    int x0 = 0;
    int y0 = 0;
    int x1 = 0;
    int y1 = 0;
    int i = 0;
    int j = 0;
    
    //mexPrintf("size_x = %d\n",size_x);
    //mexPrintf("size_y = %d\n",size_y);
    //mexPrintf("cell_size = %g\n",cell_size);
    //mexPrintf("max_range = %g\n",max_range);
    
    
    //mexPrintf("Particles = %d; scanSize = %d\n",Particles,scanSize);
    for (j = 0; j < Particles; ++j)
    {
        /* code */
        x0 = (floor((north[j])*MM / cell_size + 0.5));
        y0 = (floor((east[j])*MM / cell_size + 0.5));
        //mexPrintf("north = %g; east = %g\n",north[j],east[j]);
        //mexPrintf("x0 = %d; y0 = %d\n",x0,y0);
        for (i = 0; i < scanSize; ++i)
        {    
            x1 = (floor((north[j]*MM + max_range * cos(psi[j] + theta[i])) / cell_size + 0.5));
            y1 = (floor((east[j]*MM + max_range * sin(psi[j] + theta[i])) / cell_size + 0.5));
            //mexPrintf("x1 = %d; y1 = %d\n",x1,y1);
    //         mexPrintf("%d %d %d %d\n",x0,y0,x1,y1);
            z[i + scanSize*j] = 1 * raycast(map,cell_size,size_x,size_y,x0,y0,x1,y1,max_range);
            //mexPrintf("theta = %g; z = %g\n",theta[i], z[i + scanSize*j]);
    //         Range[i] = pose[2]+Angles[i];
        }
    }
    //mexPrintf("i = %d; j = %d\n",i,j);
     
    return;       
}