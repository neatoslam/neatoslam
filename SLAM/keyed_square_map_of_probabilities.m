% Takes in the size dimension of the map and then outputs a log probability
% map of a simple square where the 'hits' have a probability of log(0.9)
% and the 'empty space' have a probability of log(0.1) and everything else
% is set as log(0.5)

function M=keyed_square_map_of_probabilities(size)

M=log(0.5)*ones(size,size);         %Originally sets all squares to be a probability of log(0.5)

Lower = round(size/4);
Upper = round(3*size/4);
Key_x_Lower=round(4*size/10);
Key_x_Upper=round(6*size/10);
Key_y_Lower=Upper;
Key_y_Upper=round(9*size/10);


for i=Lower:Upper
    M(Lower,i)=log(0.9);            %Draws a vertical hit line one quarter of the length across
    M(i,Lower)=log(0.9);            %Draws a Horizontal hit line one quarter of the length down
    M(i,Upper)=log(0.9);            %Draws a Horizontal hit line three quarter of the length down
end


for i=Lower:Key_x_Lower     %This is the split top line
    M(Upper,i)=log(0.9);        %This is the first line
end

for i=Key_x_Upper:Upper     %This is the split top line
    M(Upper,i)=log(0.9);        %This is the first line
end

for i=Key_x_Lower:Key_x_Upper       %This is the keyway upper line
    M(Key_y_Upper,i)=log(0.9);
end

for i=Key_y_Lower:Key_y_Upper         % This is the two vertical lines for the keyway
    M(i,Key_x_Lower)=log(0.9);                  % 
    M(i,Key_x_Upper)=log(0.9);                  % 
end

for j=((Lower)+1):((Upper)-1)
    for i=((Lower)+1):((Upper)-1)       %Fills remaining squares in with log(0.1) probability to
        M(i,j)=log(0.1);                %indicate a free space
    end
end

for j=Upper:Key_y_Upper-1
    for i=Key_x_Lower+1:Key_x_Upper-1       %Fills remaining squares in with log(0.1) probability to
        M(j,i)=log(0.1);                %indicate a free space
    end
end

