function [mu,S] = UKF(mu,S,u,y,processModel,measurementModel,param,stateConstraint)

%
% Input arguments
%
% mu: current state        (n x 1)
% u:  current input        (m x 1)
% y:  current measurement  (p x 1)
%
% S:  Upper Cholesky factor of estimation error covariance (n x n)
%
% processModel:     function handle to process model
% measurementModel: function handle to measurement model
% param:            parameters to pass to process and measurement models
% stateConstraint:  function handle to state constraints (optional)
%

% Apply unscented transform through process model
%
% (mu,P) |---> (mux,Pxx)
%
processFunc = @(x) processModel(x,u,param);
if nargin >= 8
    constraintFunc = @(x) stateConstraint(x,param);
else
    constraintFunc = @(x) x;
end
[mu,S] = unscentedTransform(mu,S,processFunc,constraintFunc);

% Apply constraints to unscented mean (optional)
if nargin >= 8
    mu = stateConstraint(mu,param);
end

% Apply unscented transform through the measurement model augmented with
% the identity function
%
% (mux,Pxx) |---> ([h(mux,u)] [Pyy+R Pyx])
%                 ([  mux   ],[Pxy   Pxx])
%
jointFunc = @(x) augmentIdentityAdapter(measurementModel,x,u,param);
[muyx,Syx] = unscentedTransform(mu,S,jointFunc);

% Condition measurement and state prediction joint Gaussian on current
% measurement
[mu,S] = conditionGaussianOnMarginal(muyx,Syx,y);

% Apply constraints to corrected mean (optional)
if nargin >= 8
    mu = stateConstraint(mu,param);
end
