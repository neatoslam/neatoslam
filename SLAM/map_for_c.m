% Transforms a matlab grid matrix of probabilities into an array of 
% binary values (1 = occuppied; 0 = free)
% with x and y sizes for use in mexing.

function [M_c, size_x, size_y]  = map_for_c(M)

size_x = length(M(1,:));
size_y = length(M(:,1));

M_c = zeros(size_y*size_x, 1);


for i = 1:size_x
    
    M_c((i*size_y - size_y + 1):(i*size_y), 1) = exp(M(:,i));

end



 

















































   %   %   %
    %  %  %
     % % %
      % %
      % %
      %%% 
     %   %
     %   %
     %   %
     %   %
     %   %
     %   %
     %   %
     %   %
     %   %
     %   %
  %%%%   %%%%
 %    % %    %
%      %      %
%      %      %
 %    % %    %
  %%%%   %%%%