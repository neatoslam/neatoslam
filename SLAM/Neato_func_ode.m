function dx = Neato_func_ode(t,x,param)

% Inputs (Some function of t)
% param.wheelL = 8;%abs(5*cos(t*pi/2));%Left Wheel Angular Velocity
% param.wheelR = 8;%abs(5*sin(t*pi/2));%Right Wheel Angular Velocity

u = [param.wheelL; param.wheelR];
 
dx = Neato_func(u,x,param);
 