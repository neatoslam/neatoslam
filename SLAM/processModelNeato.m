function [xnext,A,SQ] = processModelNeato(pose,u,param)
%#codegen

%parameters
dt = param.dt; %time step
Wb = param.Wb; %wheel base
Wr = param.Wr; %wheel radius
L  = param.L;  %length to sensor

%states
north   = pose(1,:);
east    = pose(2,:);
psi     = pose(3,:);

%inputs
wheelL = u(1);
wheelR = u(2);

dx = Neato_func(u,pose,param);

% This is the Euler discretisation
xnext = pose + dt*dx;

% This is the linearisation about x (for EKF)
A = [0, 0, 0;...
     0, 0, 0;...
     0, 0, 0];

% Square root of process noise covariance
SQ = diag([1e-2,1e-2,deg2rad(1)]);