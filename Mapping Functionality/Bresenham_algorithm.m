%% Single iteration of Bresenham's line
function [] = Bresenham_algorithm(x0,y0,x1,y1,SIZEX)
% Size of square map in pixels
SIZEY=SIZEX;

% Setting colour sche,e
colormap(flipud(bone));

% Initialising Occupancy grid map
D = ones(SIZEY,SIZEX)*0.5;

% Coordinates of scanner (x0,y0) and object (x1,y1)
D(y0,x0)=0.01;
D(y1,x1)=1;

% Drawing line to represent laser scan
xline = [x0 x1];
yline = [y0 y1];
%% Testing an error case
if x0==x1 && y0==y1
   pcolor(D) 
end

%% 
dx = x1-x0;
if dx<0, sx=-1; 
else, sx=1;
end

dy = y1-y0;
if dy<0, sy=-1;
else, sy=1;
end

% If gradient is less than 1
if abs(dy)<abs(dx)
    slope = dy/dx;
    pitch = y0 - slope*x0;
    
    while x0 ~= x1
      % Changing odds of pixels to free space
      D(round(slope*x0+pitch),x0)=0.01;
      D(round(slope*x0+pitch-0.5),x0)=0.01;
      x0 = sx+x0;
    end

% If gradient is greater than or equal to 1
else
    slope = dx/dy;
    pitch = x0 - slope*y0;
   
    while y0 ~= y1
        % Changing odds of pixels to free space
        D(y0,round(slope*y0 + pitch-0.5))=0.2;
        D(y0,round(slope*y0 + pitch+0.25))=0.2;
        y0 = sy+y0;
    end
end

% Showing map of bresenhams line
pcolor(D)
mapshow(xline,yline);