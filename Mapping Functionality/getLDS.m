function [theta,range] = getLDS(s)
    n=360;                          %There will be 360 data entries for both theta and distance
    theta=zeros(n,1);               %initialise a zero array for theta
    range=zeros(n,1);               %initialise a zero array for range
    intensity=zeros(n,1);           %initialise a zero array for intensity
    error=zeros(n,1);               %initialise a zero array for error code
    str=cell(n,1);                  %initialise a cell array for raw data to go into to then be split
    fprintf(s,'SetLDSRotation on'); %Begins rotating the LDS
    fscanf(s);                      %Removes unwanted echo
    fprintf(s,'GetLDSScan');        %Send command to Serial
    pause(0.01);
    fscanf(s);                      %Removes unwanted echo
    fscanf(s);                      %Removes unwanted header
    
    for i = 1:n
        str{i} = fscanf(s);         %Runs through the data retruned from NEATO and saves into cell
        C=strsplit(str{i},',');     %Splits the data returned by commas and saves them as two seperate cell parameters in C
        if(~isempty(C))
            theta(i)=str2double(C{1});      %Extract first value from C and save it as a double for theta
            range(i)=str2double(C{2});      %Extract second value from C and save it as a double for range
            intensity(i)=str2double(C{3});  %Extract third value from C and save it as a double for intensity
            error(i)=str2double(C{4});      %Extract forth value from C and save it as a double for error
        end
    end
    
    fscanf(s);                      %Removes another unwanted data set
    
end
