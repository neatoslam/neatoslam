%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   MAIN SCRIPT FOR GRID MAPPING                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Size of square grid
size = 300;

colormap(flipud(bone));

%Robot pose, (Process model), North and East Coordinates
N0 = size/2;
E0 = size/2;
PSI0 = 0;

% Define grid-map matrix, this represents the probabilities of each
% occupied space in each square. 1=occupied, 0=free-space.
M = ones(size)*log10(0.4);
buttons=zeros(1,6);
state=0;                            % Initialise button pressed state as off
s=initialiseSerialComs('COMx');     % CHANGE x to appropriate number for com port used
TestModeOn(s);                      % Switching the NEATO into test mode

%% DATA, this is the states and measurements, these should come from NEATO and the process model


while(state~=1)
    
    [theta_read,range_read]=getLDS(s);              %Retrieves values from the hardware for range and angle
    
    
    for i=1:360                                     %Iterates through all readings
        theta = theta_read(i)-PSI0+90;
        r=round(range_read(i)/25.4);                %Sets range to be in inches
        
        % Eliminating max range measurements, note this dimention is in mm
        if r > 2000
            r=0;
        end
        
        % Calling function to compute free and occupied grid spaces from the measurement model
        [Myfree,Mxfree,Myocc,Mxocc] = free_occ_coord(N0,E0,r,theta);

        % Calling function to update grid occupancies
        M = Map_update(Myfree,Mxfree,Myocc,Mxocc,M,size); 
    end
    
    buttons=getButtons(s);                          %Retrieves values from hardware for button states
    
    %Stop when button on the bottom of screen is pressed
    if buttons(1)==1
        spin_motors(s,0,0,0,0,0,0)
    end
    %Go backward when up screen button is pressed
    if buttons(2)==1
        spin_motors(s,-200,15,80,-200,15,80);
        pause(5);                   % Pauses LDS scan until robot has finished moving
        N0=round(N0-200/25.4);      % Updating robot position
        
    end
    %When home button is pressed, state goes high and stops the plotting program
    if buttons(3)==1
        state=1;                        
    end
    
    %Go forward when down screen button is pressed
    if buttons(5)==1
        spin_motors(s,500,15,80,500,15,80);
        pause(7);                   % Pauses LDS scan until robot has finished moving
        N0=round(N0+500/25.4);      % Updating robot position
    end
       
    h = pcolor(M);                         % Plots the occupancy map
    set(h, 'EdgeColor', 'none');           % Removes the grid lines
    ylabel('NORTH');
    xlabel('EAST');
    mapshow(E0,N0,'Marker','x');           % Shows position of the robot
    
end

CloseSerialComs(s);                        %Cleanly closes Serial Connection
