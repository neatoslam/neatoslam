%% Outputs coordinates of bresenham line from inputs of robot position (x0,y0) and laser data (r,theta)
function [Myfree,Mxfree,Myocc,Mxocc]= free_occ_coord(E0,N0,r,theta,size)


% Converting from polar to cartesian coordinates
N1 = round(r*cos(deg2rad(theta)))+N0;
E1 = round(r*sin(deg2rad(theta)))+E0;


% Coordinates of object
Mxocc = N1;
Myocc = E1;


% Case to handle hits outside of area
if N1>=size
    Myocc = N1;
elseif E1>=size
    Mxocc = E1;
elseif N1<=1
    Myocc = 1;
elseif E1<=1
    Mxocc = 1;
end

%% Testing an error case
if N0==N1 && E0==E1
   Myfree = Myocc;
   Mxfree = Mxocc;
   return
end

%% 
dx = N1-N0;
if dx<0, sx=-1; 
else, sx=1;
end

dy = E1-E0;
if dy<0, sy=-1;
else, sy=1;
end
i=1;
% If gradient is less than 1
if abs(dy)<abs(dx)
    slope = dy/dx;
    pitch = E0 - slope*N0;
    
    while N0 ~= N1
      Myfree(i) = round(slope*N0+pitch-0.5);
      Mxfree(i) = N0;
      i=i+1;
     
      Myfree(i) = round(slope*N0+pitch);
      Mxfree(i) = N0;
      i=i+1;
      N0 = sx+N0;
    end

% If gradient is greater than or equal to 1
else
    slope = dx/dy;
    pitch = N0 - slope*E0;
    
    while E0 ~= E1
        Myfree(i) = E0;
        Mxfree(i) = round(slope*E0 + pitch-0.5);
        E0 = sy+E0;
        i=i+1;
    end
end