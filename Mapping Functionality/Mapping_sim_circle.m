%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   MAIN SCRIPT FOR SIMULATED GRID MAPPING              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Size of square grid
size = 100;

colormap(flipud(bone));

% Define grid-map matrix, this represents the probabilities of each
% occupied space in each sqaure. 1=occupied, 0=free-space.
M = ones(size)*log10(0.4);

%% Section 2; Robot location, This can be altered
x0 = size/2;
y0 = size/2;

%% Section 3; Run this section repeatedly to see occupancy map updated.
% Here a circular pattern of range readings is made to imitate real LDS
% readings

theta_read=1:360;                           % Array of measurement angles
range_read=1000*ones(1,360);                % Retrieves values from the hardware for range and angle

for i=1:length(theta_read)                  %Iterates through all readings
    theta = theta_read(i);
    r=round(range_read(i)/25.4);            %Sets range to be in inches

    % Calling function to compute free and occupied grid spaces
    % (Bresenham's line)
    [Myfree,Mxfree,Myocc,Mxocc] = free_occ_coord(x0,y0,r,theta,size);
      
    % Calling function to update grid occupancies in the matrix M, deals
    % with changing log odds
    M = Map_update(Myfree,Mxfree,Myocc,Mxocc,M,size); 
end

h = pcolor(M);                         % Plots the occupancy map
set(h, 'EdgeColor', 'none');           % Removes the grid lines
mapshow(x0,y0,'Marker','x');           % Shows position of the robot
