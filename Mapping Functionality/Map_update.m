%% Updates map matrix M given free and occupied coordinates
function [M]=Map_update(Myfree,Mxfree,Myocc,Mxocc,M,size)

gridnum = length(Myfree);

% Free spaces
for i=1:gridnum
    % Cases to handle free spaces outside of the grid
    if Mxfree(i)<1
        Mxfree(i) = 1;
        break           % This exits the for loop
    elseif Mxfree(i) > size
        Mxfree(i) = size;
    end
    
    if Myfree(i)<1
        Myfree(i) = 1;
        break           % This exits the for loop
    elseif Myfree(i) > size
        Myfree(i) = size;
    end
    
    if M(Myfree(i),Mxfree(i)) > log10(0.1)
        M(Myfree(i),Mxfree(i)) = M(Myfree(i),Mxfree(i)) - log10(1.2);
    else
        M(Myfree(i),Mxfree(i)) = M(Myfree(i),Mxfree(i));
    end
end
%% Cases to handle occupancies outside the grid
if Myocc < 1
    Myocc = 1;
elseif Myocc > size
    Myocc = size;
end 

if Mxocc < 1
    Mxocc = 1;
elseif Mxocc > size
    Mxocc = size;
end
%% Occupied spaces
if M(Myocc,Mxocc)<log10(0.9)
    M(Myocc,Mxocc) = M(Myocc,Mxocc) + log10(1.2);
else
    M(Myocc,Mxocc) = M(Myocc,Mxocc);
end
