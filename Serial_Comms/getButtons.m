function [buttons] = getButtons(s)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                                   Button Configurations:                                  %
    %               names=[Soft Key, Scroll Up, Start, Back, Scroll Down, Spot]                 %     
    %   Identification=[under LCD, Up arrow,Home Button, Left Arrow, Down Arrow, Grid Button]   % 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    n=6;                          %There will be 6 data entries
    buttons=zeros(n,1);           %initialise a zero array for button states
    str = cell(n,1);
    fprintf(s,'GetButtons');      %Send command to Serial
    pause(0.01);
    fscanf(s);                      %Removes unwanted echo
    fscanf(s);                      %Removes unwanted header
    for i = 1:n
        str{i} = fscanf(s);         %Runs through the data retruned from NEATO and saves into cell
        C=strsplit(str{i},',');     %Splits the data returned by commas and saves them as two seperate cell parameters in C
        if(~isempty(C))
            buttons(i)=str2double(C{2});      %Extract second value from C and save it as an integer for button state
        end  
    end
    buttons=buttons';               %Turns it into a horizontal vector -> Very Nice 
end
