function VacuumToggle(s)
    persistent state;
    if state==0
        cmd=sprintf('setmotor Vacuumoff');
        fprintf(s,cmd);
        fscanf(s);
        disp(fscanf(s));
        state=1;
    else
        cmd=sprintf('setmotor Vacuumon');
        fprintf(s,cmd);
        fscanf(s);
        disp(fscanf(s));
        state=0;
    end
return
