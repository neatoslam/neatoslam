function s=initialiseSerial(com)
    % Delete all serial objects
    newobjs = instrfind;
    delete(newobjs);
    baud=115200;
    s = serial(com, 'BaudRate', baud, 'Timeout', 1);
    s.ReadAsyncMode='continuous';
    s.InputBufferSize=64000;
    s.Terminator = 'LF';
    fopen(s);
end
