function spin_motors(s,dist1,rpm1,w1,dist2,rpm2,w2)

if dist1~=0
    cmd=sprintf('SetMotor RWheelEnable LWheelEnable');
    fprintf(s,cmd);
    fscanf(s);
    disp(fscanf(s));
    
    cmd=sprintf('SetMotor RWheelDist %d %d %d LWheelDist %d %d %d',[dist1 rpm1,w1,dist2 rpm2,w2]);
    fprintf(s,cmd);
    fscanf(s);
    disp(fscanf(s));
else
    cmd=sprintf('SetMotor RWheelDisable LWheelDisable');
    fprintf(s,cmd);
    fscanf(s);
    disp(fscanf(s));
end

%     cmd=sprintf('SetMotor RWheelDist %d %d %d LWheelDist %d %d %d',[dist1,rpm1,w1,dist2,rpm2,w2]);
%     fprintf(s,cmd);
%     fscanf(s);
%     disp(fscanf(s));
return
