%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Script runs through initialisation sequence and turns on test mode    %
%   then enters the main program. It will run in this program until any   %
%   of the buttons are pressed (can be changed to a specific button if    %
%   required, can set a different task to each of the 4 buttons). Inside  %
%   the program it cycles through reading the data from the LDS and then  %
%   plots them on a polar plot and updates in real time. Exits when a     %
%   button is pressed.                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
s=initialiseSerialComs('COM3');
TestModeOn(s);

theta=zeros(360,1);
range=ones(360,1);
buttons=zeros(1,6);
state=0;                        %Initialise button pressed state as off
while(state~=1)                    %Runs through until the any button is pressed
    [theta,range] = getLDS(s);
    PlotLDS(theta,range);
    buttons=getButtons(s);
    for i=1:6
        if buttons(i)==1
            state=1;            %When a button is pressed, state goes high and stops the plotting program
        end
    end
end
CloseSerialComs(s);