# NeatoSLAM

Simultaneous Location and Mapping on a Neato Robotic Vacuum

### Needed MATLAB Add-ons:

1. MATLAB Support for MinGW-w64 C/C++ Compiler.
 - for use for MEXing 
 - Run 'mex -setup -c' in command line if not already done so.
 - Otherwise change top sections of scripts for use with your choosen compiler.
2. Mapping Toolbox
 - Used for Grid Map Visualisation.

## Serial Communications: /Serial_Comms/

To manually communicate with the hardware follow these steps:

1. Run 's=initialiseSerialComs('COMx');' where x is the COM port the robot is 
connected to your PC.
2. Run TestModeOn(s)
3. Any of the available functions can then be run with the serial coms variable s
4. If you want to run a command that hasn't got a dedicated function, this can be 
done using the command function as follow: command(s,'xxx') where xxx is where the 
command string is put.
5. When finished manually operating the hardware run 'CloseSerialComs(s);' to cleanly 
close serial communications with the hardware. It can now be safely unplugged from USB

### Serial_Comms/Mapping_Script.m

To run this script:

1. Ensure the hardware is connected with correct drivers installed.
2. Ensure that the correct port is defined in line 10. This should iterate through and 
plot a visualisation of the LDS readings from hardware. 
3. To stop the script running, press any of the buttons on the robot.


## Mapping: /Mapping Functionality/

Code in this folder allows simulation of mapping and bresenhams line as well as 
a hardware demonstration of mapping using bresenhams line and an occupancy grid.

### Mapping Functionality/Bresenham_algorithm.m
This script is a simple demonstration of the bresenham's line algorithm used for
mapping. To run this script;

1. Run 'Bresenham_algorithm(x0,y0,x1,y1,size)' where; (x0,y0) is the position of 
the laser scanner, (x1,y1) is the position of the occupied space and (size) is the
size of the square grid.
2. Coordinates of the grid ranges from 1 to size
3. Ensure all (x0,y0,x1,y1) is between 1 and size
4. Output is a occupancy grid with a grey scale representing free space, occupied
space and uncertain space.
5. The blue displayed represents the path of the line.

example:>> Bresenham_algorithm(1,1,20,27,1);

### Mapping Functionality/Mapping_sim_circle.m
This is to demonstrate a full 360 degree LDS dataset and how saturated probabilities
are used, no hardware is used. To run this script;

1. Press 'Run'
2. This will display a occupancy grid with a circle around the robots position
3. Re-run section 3 of the script to update the probabilities. Repeat this
step until probabilites are saturated.
4. To change the position of the scanner edit and re-run section 2, now repeat step 3
to see the map update

### Mapping Functionality/Mapping_with_Hardware.m
This script is used to demonstrate mapping on hardware localising by motor inputs.
To run this script;

1. Connect the NEATO robot to the computer
2. Change line 19 of code "s=initialiseSerialComs('COMx');" to relevant com port used,
x is the port number used
3. Run the script which will open a occupancy grid map
4. Next to the LCD screen is a set of buttons: the flat bar is used to stop the NEATO
(this won't update position). The forward and backward buttons are used to move the
NEATO back and forth.
5. Press the forward/backward button to move the robot, it will come to a stop after a 
set distance and update the map and NEATO's location denoted by 'x'.
6. NOTE the map won't update until the robot has stopped moving.
7. Step 5 can be repeated for more mapping.
8. To exit the program hold down the home icon of the NEATO and it will close serial
coms and exit test mode.


## Localisation: /Localisation/

### Localisation/neato_sim.m

This script tests and runs through simulated Neato process, measurements, and localisation.

1. Housekeeping: 
Clears the workspace, initialises the mexing functions and runs all the tests.

2. Make a Map:
Creates a simulation map of 100"x100" and associated parameters.

3. Function Sim:
Defines Neato parameters and does a ODE simulation of both demanded inputs and simulated "Actual" inputs
Plots States, and NE path on top of map in (2).

4. Simulated Measurements:
Creates simulated measurements of path from (3) and adds gaussian noise.

5. UKF Initialisation
Addtional parameters for UKF estimation

6. Particle Filter Initialisation
Addtional parameters for PF estimation
note: lower particles for faster simulation at the cost of sometimes not working.

7. UKF and PF calculations
Carries out State Estimation.

8. Plot Figures
Plots state estimation (mean of PF) against 'actual' states.

9. Animation
Animates path (in inches)

### Localisation/ray_cast_testing.m

Visual Testing for Ray Casting, comparing map simulation with plotted measurements

## SLAM: /SLAM/

### SLAM/sim_SLAM.m

note: If not done so already, ensure minGW-w64 is installed through MATLAB and the code: 'mex -setup -c'
has been run to enable the compiler. To see simulation animation, run sim_SLAM.m

1. First block clears the workspace, initialises the mexing functions and runs all the tests.

2. Second block intialises all parameters used in the scripts

3. Third block returns 360 range/angle readings from the initial pose

4. Fourth block plots the new map after the inital raycast readings have been returned

5. Fifth block performs the initial localisation step using the UKF and plots a new figure that 
   positions it on the new map

6. Sixth block is where SLAM is performing. Here the following steps are iterated between to build up 
   a new map and move through it:
            i.   Move forward a timestep
            ii.  Get a new set of range measurements from the raycast function
            iii. Iterate through range measurements and build up map
            iv.  Localise inside of this new map
            v.   update figure
