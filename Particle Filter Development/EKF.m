function [mu,S] = EKF(mu,S,u,y,processModel,measurementModel,param)

%
% Input arguments
%
% mu: current state        (n x 1)
% u:  current input        (m x 1)
% y:  current measurement  (p x 1)
%
% S:  Upper Cholesky factor of estimation error covariance (n x n)
%
% processModel:     function handle to process model
% measurementModel: function handle to measurement model
% param:            parameters to pass to process and measurement models
%

% Apply affine transform through process model
%
% (mu,P) |---> (f(mu,u),A*P*A.') = (mux,Pxx)
%
processFunc = @(x) processModel(x,u,param);
[mu,S] = affineTransform(mu,S,processFunc);

% Apply affine transform through the measurement model augmented with
% the identity function
%
% (mux,Pxx) |---> ([h(mux,u)] [Pyy+R Pyx])
%                 ([  mux   ],[Pxy   Pxx])
%
jointFunc = @(x) augmentIdentityAdapter(measurementModel,x,u,param);
[muyx,Syx] = affineTransform(mu,S,jointFunc);

% Condition measurement and state prediction joint Gaussian on current
% measurement
%
% 
%
[mu,S] = conditionGaussianOnMarginal(muyx,Syx,y);
