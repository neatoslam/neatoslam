function [y,C,SR] = measurementModelMassDamper(x,u,param)
%#codegen

y = x(1,:);

% This is the linearisation about x
C = [1, 0, 0];

% D = 0;

% Measurement noise covariance
SR = param.SR;

