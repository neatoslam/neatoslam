
% Model parameters
param.m = 1;            % Mass [kg]

% Input parameters
param.a = 1;          	% Amplitude [N]
param.f = 0.2;        	% Frequency [Hz]

% Simulation parameters
T = 0.01;
t = 0:T:120;
options = odeset('MaxStep',0.1);

% Set initial condition
x0 = [ ...
    5; ...              % Initial position
    1; ...              % Initial velocity
    0.1 ...             % Damping coefficient
    ];

% Run the simulation
[t,x] = ode45(@(t,x) massdamper_fun(t,x,param),t,x0,options);

%% Simulated measurements

m = 1;

sigmaR = 0.1;       % stdev of sensor

yHist = x(:,1) + sigmaR*randn(length(t),1);

% Addtional parameters for state estimation
param.T = T;
param.SQ = diag([0.003,0.003,0.0001]);
param.SR = sigmaR;

% Initial state
x0est = [ ...
    5; ...              % Initial position
    0.8; ...              % Initial velocity
    0.2 ...             % Damping coefficient
    ];

mu = x0est;

% Initial estimation error covariance
S = diag([1,1,0.03]);

%% Initialising Weightings
% Randomly scatter M number of particles throughout the possible lengths

M=1000;                            % Number of particles being assigned
x_0=zeros(3,M);                     % Initialise the matrix of the initial state

for i=1:M                           % Sets the random states for the initial positions
    x_0(1,i)=rand*25;               % Sets random position between 0 and 25 m
    x_0(2,i)=rand*3.5-1;            % Sets random velocity between -1 and 2.5m/s
    x_0(3,i)=rand*0.4-0.1;          % Sets random Damping Coefficient between 0 and 0.2   
end

x_k=x_0;                            % initial state positions
log_omega_k=log((1/M))*ones(1,M);   % log weights initialised

%% Initialise the plotting arrays

muHist = zeros(length(x0est),length(t));
pos_part_hist=zeros(M,length(t));
vel_part_hist=zeros(M,length(t));
dmp_part_hist=zeros(M,length(t));

%% Timestep iterations for EKF and Bootstrapped Particle Filter

for i = 1:length(t)
    u = param.a*sin(2*pi*param.f*t(i));
    y = yHist(i);
    
    [mu,S] = EKF(mu,S,u,y,@processModelMassDamper,@measurementModelMassDamper,param);
    [x_k,log_omega_k] = PF(x_k,log_omega_k,u,y,@processModelMassDamper,@measurementModelMassDamper,param);
    
    pos_part_hist(:,i)=x_k(1,:);
    vel_part_hist(:,i)=x_k(2,:);
    dmp_part_hist(:,i)=x_k(3,:);
    muHist(:,i) = mu;
end

position_particle_history=mean(pos_part_hist);
velocity_particle_history=mean(vel_part_hist);
damping_particle_history=mean(dmp_part_hist);

%% Plotting Figures

% Plot figures
figure(2);clf
subplot(3,1,1)
plot(t,x(:,1),t,muHist(1,:),t,position_particle_history,'Linewidth',2)
ylabel('Position [m]')
legend('True','Estimated','Particle Filter')
grid on
subplot(3,1,2)
plot(t,x(:,2),t,muHist(2,:),t,velocity_particle_history,'Linewidth',2)
ylabel('Velocity [m/s]')
legend('True','Estimated','Particle Filter')
grid on
subplot(3,1,3)
plot(t,x(:,3),t,muHist(3,:),t,damping_particle_history,'Linewidth',2)
xlabel('Time [s]')
ylabel('Damping coefficient')
legend('True','Estimated','Particle Filter')
grid on
