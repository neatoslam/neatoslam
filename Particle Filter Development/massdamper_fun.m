function dx = massdamper_fun(t,x,param)

m = param.m;

a = param.a;          % Amplitude [N]
f = param.f;          % Frequency [Hz]
u = a*sin(2*pi*f*t);

dx = [ ...
    x(2); ...                   % Kinematic relation
    (u - x(2)*x(3))/m; ...      % Acceleration due to damping and input
    0 ...                       % Damping coefficient is constant
    ] ;