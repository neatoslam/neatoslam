function [xnext,A,SQ] = processModelMassDamper(x,u,param)
%#codegen

T = param.T;
m = param.m;
M = size(x,2);
% This is the Euler discretisation
xnext = x + T*[x(2,:); (u - x(3,:).*x(2,:))/m; zeros(1,M)];

% This is the linearisation about x
A = [1, T, 0; ...
    0, 1 - T/m*x(3), -T/m*x(2); ...
    0, 0, 1];

% B = [0; T/m; 0];

% Process noise covariance
SQ = param.SQ;