function x = constraintTrack2D(x, param)

% Enforce lower bound on coefficient of drag.
x(5) = max([x(5),0.01]);

% Enforce upper bound on coefficient of drag
x(5) = min([x(5),2]);