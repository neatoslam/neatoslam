%% 1. Housekeeping

clear;clc;close all

profile on

mex Mainmex.c raycast.c
% Run unit tests for function
testResults = runtests('Neato_func_test.m');
assert(all([testResults(:).Passed]))

%% 2. Make a Map

mapSize = 100; %in grid squares
mapSizeM = mapSize*25.4/1000; %in metres

param.map = keyed_square_map_of_probabilities(mapSize);%grid_map(mapSize, "keyway");
param.max_range = 1400*mapSize;
param.cell_size = 25.4; %mm = 1 inch

figure(1);clf
colormap(flipud(bone));                % Sets colour scheme to be black and white
h = pcolor(param.map);                         % Plots the occupancy map
set(h, 'EdgeColor', 'none');           % Removes the grid lines
title('Generated Map for Simulation (number of grid squares)');
hold on
[param.mapC, param.size_x, param.size_y] = map_for_c(param.map);

%% 3. Function Sim

% Model parameters
param.Wb = 0.015;           %Wheel Base
param.Wr = 0.005;           %Wheel Radius
param.L = 0.03;             %Length to sensor

param.wheelL =  15;
param.wheelR =  15;

param.random = 0;

% Simulation parameters
T = 0.1;
t = 0:T:20;
options = odeset('MaxStep',0.1);

% Set initial condition
northI  = 6*mapSizeM/8; ...          % Initial North
eastI   = 2*mapSizeM/8; ...           % Initial East
psiI    = deg2rad(135);

pose0 = [ ...
        northI; ...         % Initial north
        eastI; ...          % Initial east
        psiI;  ...          % Initial psi
     ];

% Run the 'demanded' simulation
[t,xDemand] = ode45(@(t,x) Neato_func_ode(t,x,param),t,pose0,options);
param.dt = t(2) - t(1);

param.random = 1;
% Run the 'actual' simulation
[t,x] = ode45(@(t,x) Neato_func_ode(t,x,param),t,pose0,options);
param.dt = t(2) - t(1);

xInches  = x(:,1:2).*1000/param.cell_size;
xDInches = xDemand(:,1:2).*1000/param.cell_size;

figure(1);
plot(xDInches(1,2),xDInches(1,1), 'x');
title('Neato Path in World Coordinates (Demanded)')
hold on
plot(xDInches(:,2),xDInches(:,1),xInches(:,2),xInches(:,1))
ylabel('North [m]') 
xlabel('East [m]')
% plot(eastI + param.L*sin(psiI), northI + param.L*cos(psiI), 'x');
% plot(xDInches(:,2) + param.L*sin(xDemand(:,3)),xDInches(:,1) + param.L*cos(xDemand(:,3)),...
%     xInches(:,2) + param.L*sin(x(:,3)), xInches(:,1) + param.L*cos(x(:,3)))
legend('Map','Sensor Start', 'Demanded Sensor Path', 'Actual Sensor Path');%, 'Axle Centre Start', 'Demanded Axle Centre Path', 'Actual  Axle Centre Path')


% Plotting
figure(2);clf
subplot(3,1,1)
plot(t,xDemand(:,1),t,x(:,1))
legend('Demanded', 'Actual')
title('Neato Demanded State Plots')
xlabel('Time [s]')
ylabel('North [m]')
grid on

subplot(3,1,2) 
plot(t,xDemand(:,2),t,x(:,2))
xlabel('Time [s]') 
ylabel('East [m]')
grid on

subplot(3,1,3)
plot(t,rad2deg(xDemand(:,3)),t,rad2deg(x(:,3)))
xlabel('Time [s]')
ylabel('Psi [deg]')
grid on


%% 4. Simulated measurements

nt  = length(t);
u   = [0;0];

[y,~,SR] = measurementModelNeato(pose0,u,param);
ny      = length(y);
yHist   = zeros(ny, nt);
for i = 1:nt
    xtrue       = x(i,:)';
    yHist(:,i)  = measurementModelNeato(xtrue,u,param) + SR'*randn(ny,1);
    
    %measurementModelNeato(xtrue,u,param);
end


%% 5. UKF Initialisation
% Addtional parameters for state estimation
param.dt = T;

% Initial state
mu0 = [ ...
        northI - 500e-3; ...          % Initial North
        eastI + 400e-3; ...           % Initial East
        psiI + deg2rad(10) ...           % Initial psi
        ];

muUKF = mu0;

% Initial estimation error covariance
S = diag([0.5,0.5,deg2rad(10)]); % Upper Cholesky factor
assert(isequal(S,triu(S)));
SUKF = S; % Upper Cholesky decomposition


muHistUKF = zeros(length(muUKF),length(t));
SdiagHistUKF = zeros(length(muUKF),length(t)); 

%% 6. Particle Filter Initialisation

%Particles can be lowered to speed up run time for markers. Sometimes doesn't work
%though
M=10000;                            % Number of particles being assigned
x_0=zeros(3,M);                     % Initialise the matrix of the initial state

% Sets the random states for the initial positions
x_0(1,:)=rand(1,M)*(6*mapSizeM/8)+(mapSizeM/8);     % Sets random North position between 1*mapsize/8 and 7*mapsize/8
x_0(2,:)=rand(1,M)*(6*mapSizeM/8)+(mapSizeM/8);     % Sets random East position between 1*mapsize/8 and 7*mapsize/8
x_0(3,:)=rand(1,M)*2*pi;                            % Sets random psi position between 0 and 2pi   

x_k=x_0;                            % initial state positions
log_omega_k=log((1/M))*ones(1,M);   % log weights initialised

% Initialise the plotting arrays
muHist = zeros(length(x_0),length(t));
N_part_hist=zeros(M,length(t));
E_part_hist=zeros(M,length(t));
Psi_part_hist=zeros(M,length(t));

%% 7. UKF and PF calculations

for i = 1:length(t)
    
    %Nah nah, I swear we're goin' straight.
    u =  [param.wheelL;
          param.wheelR];
    y = yHist(:,i);
    %UKF CALCULATIONS
    [muUKF,SUKF] = UKF(muUKF,SUKF,u,y,@processModelNeato,@measurementModelNeato,param);
    muHistUKF(:,i) = muUKF;
    SdiagHistUKF(:,i) = realsqrt(diag(SUKF.'*SUKF)); % Square root of diagonal of P
    %PF CALCULATIONS 
    [x_k,log_omega_k] = PF(x_k,log_omega_k,u,y,@processModelNeato,@measurementModelNeato,param);
    N_part_hist(:,i)=x_k(1,:);
    E_part_hist(:,i)=x_k(2,:); 
    Psi_part_hist(:,i)=x_k(3,:);
end

North_particle_history=mean(N_part_hist);
East_particle_history=mean(E_part_hist);
Psi_particle_history=mean(Psi_part_hist);

%% 8. Plot figures
figure(4);clf
subplot(3,1,1)
plot(t,xDemand(:,1),...
       t,x(:,1),...t,muHistEKF(1,:),
       t,muHistUKF(1,:),'g',...
       t,North_particle_history,'k')
xlabel('Time [s]')
ylabel('North [m]')
legend('Demanded','True','UKF','PF')
grid on
subplot(3,1,2)
plot(t,xDemand(:,2),...
    t,x(:,2),...t,muHistEKF(2,:),
    t,muHistUKF(2,:),'g',...
    t,East_particle_history,'k')
xlabel('Time [s]')
ylabel('East [m]')
legend('Demanded','True','UKF','PF')
grid on
subplot(3,1,3)
plot(t,rad2deg(xDemand(:,3)),...
    t,rad2deg(x(:,3)),...t,rad2deg(muHistEKF(3,:)),
    t,rad2deg(muHistUKF(3,:)),'g',...
    t,rad2deg(Psi_particle_history),'k')
xlabel('Time [s]')
ylabel('Psi [deg]')
legend('Demanded','True','UKF','PF')
grid on

profile viewer


%% 9. Animation
East_map_paticles=E_part_hist*1000/param.cell_size;
North_map_paticles=N_part_hist*1000/param.cell_size; 

map_north_ukf = muHistUKF(1,:)*1000/param.cell_size;
map_east_ukf =  muHistUKF(2,:)*1000/param.cell_size;

j = 1;
%figure(5)
figure(6)       % Plotting all particles 
for i=1:length(t)
   figure(6); clf       % Plotting all particles
   
   colormap(flipud(bone));   % Sets colour scheme to be black and white
   h = pcolor(param.map); % Plots the occupancy map
   set(h, 'EdgeColor', 'none');           % Removes the grid lines
  % title('Map and Path Simulation');
   plot(xDInches(1,2),xDInches(1,1), 'x');
   title('Neato Path in World Coordinates')
   hold on
   plot(xDInches(:,2),xDInches(:,1),xInches(:,2),xInches(:,1))
   ylabel('North [m]') 
   xlabel('East [m]')
    % plot(eastI + param.L*sin(psiI), northI + param.L*cos(psiI), 'x');
    % plot(xDInches(:,2) + param.L*sin(xDemand(:,3)),xDInches(:,1) + param.L*cos(xDemand(:,3)),...
    %     xInches(:,2) + param.L*sin(x(:,3)), xInches(:,1) + param.L*cos(x(:,3)))
 

   scatter(East_map_paticles(:,i),North_map_paticles(:,i),'b+')
   scatter(map_east_ukf(i),map_north_ukf(i),'r+')
   axis([0 100 0 100]);
   legend('Sensor Start', 'Demanded Sensor Path', 'Actual Sensor Path', 'Particles', 'UKF Estimation');%, 'Axle Centre Start', 'Demanded Axle Centre Path', 'Actual  Axle Centre Path')
   
% Save pictures for animation
%    filename = sprintf("path_%d.jpeg", j);
%    saveas(gcf,filename);
%    j = j+1;

   pause(eps)
end


profile off