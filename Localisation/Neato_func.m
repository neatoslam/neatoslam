function dx = Neato_func(u,pose,param)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TAKE PICTURE OF COORDINATE SYSTEM AND LINK HERE%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters
Wb = param.Wb; %wheel base
Wr = param.Wr; %wheel radius
L  = param.L;  %length to sensor

% Current States
east     = pose(1);
north    = pose(2);
psi      = pose(3);

% Inputs
wheelL = u(1);%Left Wheel Angular Velocity
wheelR = u(2);%Right Wheel Angular Velocity

% Rotation matrix of sensor basis vectors expressed in {0}
R01 = expm(psi*skew([0;0;1]));  % To {0} from {1}
% R01 = [ ...
%      cos(psi), -sin(psi), 0; ...
%      sin(psi),  cos(psi), 0; ...
%             0,         0, 1 ... 
%      ];

R10 = transpose(R01);           % To {1} from {0}

% Angular velocity vector expressed in {1}
omega101 = [0; 
            0; 
            (wheelL - wheelR)*(Wr/(2*Wb))];
% Angular velocity vector expressed in {0}
omega100 = R01*omega101;

% Position vector to B from A expressed in {1}
rBA1 = [-L;
        0;
        0];
    
% Position vector to B from A expressed in {0}    
rBA0 = R01*rBA1;

% Position vector to B from N expressed in {0}
rBN0 = [north;
        east;
        0];
% Position vector to B from N expressed in {1} 
rBN1 = R10*rBN0;

% Position vector to A from N expressed in {0}    
rAN0 = rBN0 - rBA0;

% Velocity vector to A from N expressed in {1}
vAN1 = [(wheelL + wheelR)*(Wr/2);
        0;
        0];
    
% Velocity vector to A from N expressed in {0}
vAN0 = R01*vAN1;

% Velocity vector to B from N expressed in {0}
% vBN0 = vAN0 + skew(omega100)*rBA0;

vBN1 = vAN1 + skew(omega101)*rBA1;

vBN0 = R01*vBN1;

dx = [ ...
     vBN0(1);...        northDot
     vBN0(2);...        eastDot
     omega100(3);...    psiDot
     ];
 
%dx =[0;0;0];