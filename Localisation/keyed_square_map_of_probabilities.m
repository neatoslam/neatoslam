% Takes in the size dimension of the map and then outputs a log probability
% map of a simple square where the 'hits' have a probability of log(0.9)
% and the 'empty space' have a probability of log(0.1) and everything else
% is set as log(0.5)

function M=keyed_square_map_of_probabilities(size)

%size = 80; %print at size 10*divisions to see how to adjust

divisions = 8;

M=log(0.5)*ones(size,size);         %Originally sets all squares to be a probability of log(0.5)

one     = round(1*size/divisions);
two     = round(2*size/divisions);
three   = round(3*size/divisions);
four    = round(4*size/divisions);
five    = round(5*size/divisions);
six     = round(6*size/divisions);
seven   = round(7*size/divisions);

%Outside Box
M(one,one:six)=log(0.9);          %bottom line
M(seven,one:six)=log(0.9);        %top line
M(one:seven,one)=log(0.9);        %left line

%free main square
M(one+1:seven-1, one+1:six-1) = log(0.1);

%keyway vertical
M(one:three,six)=log(0.9);
M(three:five,seven)=log(0.9);
M(five:seven,six)=log(0.9);

%keyway horizontal
M(three,six:seven)=log(0.9);
M(five,six:seven)=log(0.9);


%free keyway
M(three+1:five-1, six:seven-1) = log(0.1);

%% This is just showing what the output looks like as an occupied colour map

figure(1)
colormap(flipud(bone));                % Sets colour scheme to be black and white
h = pcolor(M);                         % Plots the occupancy map
set(h, 'EdgeColor', 'none');           % Removes the grid lines
title('Generated Map for Simulation (number of grid squares)');
