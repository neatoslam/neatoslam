function [x,y] = mapIDX(cell_size,N,E)

r_po = [N;E];
index=floor(r_po/cell_size)+[1;1];
x=index(1);
y=index(2);