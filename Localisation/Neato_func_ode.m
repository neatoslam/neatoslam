function dx = Neato_func_ode(t,x,param)

% Inputs (Some function of t)
if param.random == 1
    param.wheelL =  param.wheelL + 1*randn(1,1);
    param.wheelR =  0.98*param.wheelR + 1*randn(1,1);
end

u = [param.wheelL; param.wheelR];

dx = Neato_func(u,x,param);
 