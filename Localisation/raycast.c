/** Adapted from:
/**************************************************************************
 * Desc: Range routines
 * Author: Andrew Howard
 * Date: 18 Jan 2003
 * CVS: $Id: map_range.c 1347 2003-05-05 06:24:33Z inspectorg $
*************************************************************************
** by Zach Honan 2019-05-29 */

#include <assert.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "mex.h"
#include "map.h"

// Extract a single range reading from the map.  Unknown cells and/or
// out-of-bound cells are treated as occupied, which makes it easy to
// use Stage bitmap files.
double raycast(double *map, double cell_size, int size_x, int size_y, int x0, int y0, int x1, int y1, double max_range)
{
    //mexPrintf("In RayCast\n");
    //cell_size = 25.4;
    //max_range = 1778.0;
    
//     mexPrintf("size_x = %d\n",size_x);
//     mexPrintf("size_y = %d\n",size_y);
//     mexPrintf("cell_size = %g\n",cell_size);
//     mexPrintf("max_range = %g\n",max_range);
//     mexPrintf("x0 = %d; y0 = %d\n",x0,y0);
//     mexPrintf("x1 = %d; y1 = %d\n",x1,y1);
    
  // Bresenham raytracing
  int x,y;
  int xstep, ystep;
  char steep;
  int tmp;
  int deltax, deltay, error, deltaerr;

  if(abs(y1-y0) > abs(x1-x0))
    steep = 1;
  else
    steep = 0; 

  if(steep)
  {
    tmp = x0;
    x0 = y0;
    y0 = tmp;

    tmp = x1;
    x1 = y1;
    y1 = tmp;
  }

  deltax = abs(x1-x0);
  deltay = abs(y1-y0);
  error = 0;
  deltaerr = deltay;

  x = x0;
  y = y0;

  if(x0 < x1)
    xstep = 1;
  else
    xstep = -1;
  if(y0 < y1)
    ystep = 1;
  else
    ystep = -1;

  if(steep)
  {
    if((x <= 0) && (x >= size_x) && (y <= 0) && (y >= size_y)) 
    {
        if(map[x*size_y + y] > 0.5)
        {
            return sqrt((x-x0)*(x-x0) + (y-y0)*(y-y0)) * cell_size/1000;
        }
    }
  }
  else
  {
    if(((x <= 0) && (x >= size_x) && (y <= 0) && (y >= size_y)))
    {
        if(map[y*size_x + x] > 0.5)
        {
              return sqrt((x-x0)*(x-x0) + (y-y0)*(y-y0)) * cell_size/1000;
        }
    }
  }
  while(x != (x1 + xstep * 1))
  {
    x += xstep;
    error += deltaerr;
    if(2*error >= deltax)
    {
      y += ystep;
      error -= deltax;
    }

    if(steep)
    {
        if((x >= 0) && (x <= size_x) && (y >= 0) && (y <= size_y)) 
        {
            if(map[(y)*size_x+x] >0.5)
            {
                return sqrt((x-x0)*(x-x0) + (y-y0)*(y-y0)) * cell_size/1000;
//                 return 0.5;
            }
        }
    }
    else
    {
        if((x > 0) && (x < size_x) && (y > 0) && (y < size_y)) 
        {
            if(map[(x)*size_y + y] >0.5)
            {
                  return sqrt((x-x0)*(x-x0) + (y-y0)*(y-y0)) * cell_size/1000;
//                     return 0.5;
            }
        }
    }
  }
  return max_range;             //Should be max-range, set as 2 so i can see whats going on
}