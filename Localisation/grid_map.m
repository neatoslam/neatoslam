% Takes in the size dimension of the map (grid squares) and then outputs a log probability
% map of a simple square where the 'hits' have a probability of log(0.9)
% and the 'empty space' have a probability of log(0.1) and everything else
% is set as log(0.5)

% Shapes can be, "square" (default), "keyway", or "pathway".

function M=grid_map(size, shape)

%size = 80; %print at size 10*divisions to see how to adjust

if nargin < 2
 shape = "square";
end

divisions = 8;

rows = size;
cols = size;

if shape ~= "square"
    cols = round(size+size/divisions);
end
 
M=log(0.5)*ones(rows,cols);         %Originally sets all squares to be a probability of log(0.5)

one     = round(1*size/divisions);
two     = round(2*size/divisions);
three   = round(3*size/divisions);
four    = round(4*size/divisions);
five    = round(5*size/divisions);
six     = round(6*size/divisions);
seven   = round(7*size/divisions);
eight   = round(8*size/divisions);

%Outside Box
M(one,one:seven)=log(0.9);          %bottom line
M(seven,one:seven)=log(0.9);        %top line
M(one:seven,one)=log(0.9);        %left line

if shape == "square"
    M(one:seven,seven)=log(0.9);        %right line
else
    %keyway vertical
    M(one:three,seven)=log(0.9);
    M(three:five,eight)=log(0.9);
    M(five:seven,seven)=log(0.9);

    %keyway horizontal
    M(three,seven:eight)=log(0.9);
    M(five,seven:eight)=log(0.9);

    %free keyway
    M(three+1:five-1, seven:eight-1) = log(0.1);
end

if shape ~= "pathway"
    %free main square
    M(one+1:seven-1, one+1:seven-1) = log(0.1);
else
    %Inside Box
    M(two,two:six)=log(0.9);          %bottom line
    M(six,two:six)=log(0.9);          %top line
    M(two:six,two)=log(0.9);          %left line
    M(two:six,six)=log(0.9);         %right line

    %free left column
    M(one+1:seven-1, one+1:two-1) = log(0.1);
    %free right column
    M(one+1:seven-1, six+1:seven-1) = log(0.1);
    %free bottom row
    M(one+1:two-1, one+1:seven-1) = log(0.1);
    %free top row
    M(six+1:seven-1, one+1:seven-1) = log(0.1);
end


%% This is just showing what the output looks like as an occupied colour map
% 
% figure(1)
% colormap(flipud(bone));                % Sets colour scheme to be black and white
% h = pcolor(M);                         % Plots the occupancy map
% set(h, 'EdgeColor', 'none');           % Removes the grid lines
% title('Generated Map for Simulation (number of grid squares)');
