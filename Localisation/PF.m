% Particle filter:
% Takes in the particles pose and weightings. This the resamples the
% particles according to each of their weightings, by removing the 'bad'
% particles and moving them to the 'good' particles positions with some
% noise. It then predicts the next states of the particles, and sends
% these new states to the measurement model to get the predicted
% measurement. This measurement is then compared against the given 'actual'
% measurement and probabilities are updated accordingly. The new states and
% probabilities are then returned to the original function

function [x_k,log_omega_k] = PF(x_k,log_omega_k,u,y_k,processModel,measurementModel,param)


%% Resample
M=length(log_omega_k);
IDX=randsample(M,M,true,exp(log_omega_k));                                           % Returns weighted 

x_new=x_k(:,IDX);                                                                    % Resamples around successfull samples


%     figure(3); clf
%     hold on
%     scatter(x_k(2,:),x_k(1,:),'rx')
%     scatter(x_new(2,:),x_new(1,:),'b+')
%     axis([0 3 0 3]);
%     %hold off
%     
%% PREDICTION
   
[xnext,~,S_Q] = processModel(x_new,u,param);                                         % Retrieves the next state readings from the process model
[h,~,S_R] = measurementModel(xnext,u,param);                                         % Retrieves the measurement of position from the Measurement model

x_k = xnext + S_Q'*randn(3,M);                                                     % Puts a Gaussian on  the readings

%% CORRECTION

z=S_R'\(y_k-h);                                                                      % Difference between estimated pose and given pose

log_p=-(1/2)*sum(z.^2, 1)-sum(log(abs(diag(S_R))))-(length(y_k)/2)*log(2*pi);        % Calculated Weighted probabilities

log_omega_k=log_p-max(log_p)-log(sum(exp(log_p-max(log_p))));                        % Normalised updated Probabilities


