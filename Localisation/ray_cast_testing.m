
clear; clc; close all
profile on
mex Mainmex.c raycast.c
mapSize = round(3*1000/25.4); %in grid squares
%mapSize = round(100);
mapSizeM = mapSize*25.4/1000; %in metres
param.map = keyed_square_map_of_probabilities(mapSize);
param.max_range = 1400*mapSizeM;
param.cell_size = 25.4; %mm = 1 inch

[mapC, size_x, size_y] = map_for_c(param.map);

% Position in Metres/Radians
northI = 4*mapSizeM/8;%*ones(1,9);
eastI = 2*mapSizeM/8;%*ones(1,9);
psiI = [0];% pi/4 2*pi/4 3*pi/4 4*pi/4 5*pi/4 6*pi/4 7*pi/4 8*pi/4];

% Set initial condition
pose = [ ...
        northI; ...         % Initial north
        eastI; ...          % Initial east
        psiI;  ...          % Initial psi
       ];

figure(1)
colormap(flipud(bone));                % Sets colour scheme to be black and white
h = pcolor(param.map);                 % Plots the occupancy map
set(h, 'EdgeColor', 'none');           % Removes the grid lines
title('Generated Map for Simulation (number of grid squares)');

raygap = 10; 

%measurements
r = zeros(360/raygap,1);

theta = deg2rad(raygap:raygap:360);

z = Mainmex(size_x, size_y, param.max_range, param.cell_size, mapC', theta, northI, eastI, psiI);


for j = 1:length(pose(1,:))
    for i = 1:360/raygap

        r(i, j) = Ray_cast(pose(:,j), theta(i), param);

    end
end
%figure(2)
%hold on
%polarplot(theta,z, 'ro',theta,r, 'bo')
%polarplot(theta,r, 'bo')
    

for i=1:length(pose(1,:))
    figure(i+1);
    polarplot(theta,z(36*i-35:36*i), 'ro')
    
end

profile viewer
profile off