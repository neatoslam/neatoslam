function [y,C,SR] = fakeMeasurementModelNeato(x,u,param)
%#codegen
M=length(x);
%parameters
dt = param.dt; %time step
Wb = param.Wb; %wheel base
Wr = param.Wr; %wheel radius
L  = param.L;  %length to sensor

%states
north   = x(1,:);
east    = x(2,:);
psi     = x(3,:);

%inputs
wheelL = u(1);
wheelR = u(2);

%measurements
y = ... raycasting()
    [north;
     east;
     psi];
%y = x(1:2,:);

ny = length(y(:,1));

% This is the linearisation about x
C = [];

% Square root of measurement noise covariance
SR = 1e-2*eye(ny);

