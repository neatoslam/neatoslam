%% Having a crack at ray casting translation
%function range=Ray_cast(map,N,E,psi,max_range,cell_size)
function range = Ray_cast(pose, theta, param)

mm = 1000;

N=pose(1)*mm;       %m to mm
E=pose(2)*mm;       %m to mm
psi = pose(3);

map = param.map;
sze=length(map);
cell_size = param.cell_size;
max_range = param.max_range;

[x0,y0]=mapIDX(cell_size,N,E);       % Intial North position for robot

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Moved iterations out for troubleshooting %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%theta = 0;%deg2rad(1:360); % Measurement angle


range = max_range;%*ones(360,1);

%for i = 1%:360

    [x1,y1]=mapIDX(cell_size,N+cos(theta + psi)*max_range,E+sin(theta + psi)*max_range);


    if(abs(y1-y0)>abs(x1-x0))         
        steep=1;        %If angle is greater than 45 deg. set steep to be 0
    else
        steep=0;        %If angle is less than 45 deg. set steep to be 0
    end

    if steep == 1       %Swap the North and East readings if >45 degrees.
        tmp=x0;
        x0=y0;                  
        y0=tmp; 

        tmp=x1;
        x1=y1;
        y1=tmp;
    end

    deltax=abs(x1-x0);         
    deltay=abs(y1-y0);          

    error=0;
    deltaerr=deltay;

    x=x0;           %Initialise iterative variables
    y=y0;

    if x0<x1                              
        xstep=1;            % If object is to the right, step to the right, else step backward
    else
        xstep=-1;
    end

    if y0<y1
        ystep=1;
    else
        ystep=-1;
    end

    if steep == 1
        if y>sze && y<0 && x>sze && x<0 
            if map(x,y)>log(0.5)
                range=sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0))*cell_size/mm;
            end
        end

    else
        if y>sze && y<0 && x>sze && x<0 
            if map(y,x)>log(0.5)
                range= sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0))*cell_size/mm;
            end
        end
    end

    while x~=(x1+xstep*1)
        x=x+xstep;
        error=error+deltaerr;
        if 2*error>=deltax
            y=y+ystep;
            error=error-deltax;
        end
        if steep == 1
            if y<sze && y>0 && x<sze && x>0 
                if map(x,y)>log(0.5)
                    range=sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0))*cell_size/mm;
                end
            end

        else
            if y<sze && y>0 && x<sze && x>0
                if map(y,x)>log(0.5)
                    range=sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0))*cell_size/mm;
                end
            end
        end    
    end
    
%end